'use strict';

const app = require('express')();
const cors = require('cors');
const tasksContainer = require('./tasks.json');

//Midlewares
app.use(cors());

/**
 * GET /tasks
 * 
 * Return the list of tasks with status code 200.
 */
app.get('/tasks', (req, res) => {
  return res.status(200).json(tasksContainer);
});

/**
 * Get /task/:id
 * 
 * id: Number
 * 
 * Return the task for the given id.
 * 
 * If found return status code 200 and the resource.
 * If not found return status code 404.
 * If id is not valid number return status code 400.
 */
app.get('/task/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  if (!Number.isNaN(id)) {
    const task = tasksContainer.tasks.find((item) => item.id == id);

    if (task !== null && task !== undefined) {
      return res.status(200).json({
        task,
      });
    } else {
      return res.status(404).json({
        message: 'Requested ID Not Found.',
      });
    }
  } else {
    return res.status(400).json({
      message: 'Bad Request.',
    });
  }
});

/**
 * PUT /task/update/:id/:title/:description
 * 
 * id: Number
 * title: string
 * description: string
 * 
 * Update the task with the given id.
 * If the task is found and update as well, return a status code 204.
 * If the task is not found, return a status code 404.
 * If the provided id is not a valid number return a status code 400.
 */
app.put('/task/update/:id/:title/:description/:favourite', (req, res) => {
  const id = parseInt(req.params.id, 10);
  const favourite = parseInt(req.params.favourite, 10);
  if (!Number.isNaN(id)) {
    const task = tasksContainer.tasks.find(item => item.id === id);

    if (task !== null && task !== undefined) {
      task.title = req.params.title;
      task.description = req.params.description;
      task.favourite = favourite;
      // To give feedback of our new tasks inside the todo list.
      res.json({
        message: "Sucessfully Updated !",
        tasks: tasksContainer.tasks,
      });
    } else {
      return res.status(404).json({
        message: 'Requested ID Not Found',
      });
    }
  } else {
    return res.status(400).json({
      message: 'Bad Request',
    });
  }
});

/**
 * POST /task/create/:title/:description
 * 
 * title: string
 * description: string
 * 
 * Add a new task to the array tasksContainer.tasks with the given title and description.
 * Return status code 201.
 */
app.post('/task/create/:id/:title/:description', (req, res) => {
  const task = {
    id: parseInt(req.params.id, 10),
    title: req.params.title,
    description: req.params.description,
    favourite : 0
  };

  tasksContainer.tasks.push(task);

  return res.status(201).json({
    message: 'Resource created',
  });
});

/**
 * DELETE /task/delete/:id
 * 
 * id: Number
 * 
 * Delete the task linked to the  given id.
 * If the task is found and deleted as well, return a status code 204.
 * If the task is not found, return a status code 404.
 * If the provided id is not a valid number return a status code 400.
 */
app.delete('/task/delete/:id', (req, res) => {
  const id = parseInt(req.params.id, 10);

  if (!Number.isNaN(id)) {
    const index = tasksContainer.tasks.findIndex(function (o) {
      return o.id === id;
    })
    if (index !== -1) {
      tasksContainer.tasks.splice(index, 1);
      return res.status(200).json({
        message: 'Sucessfully Deleted !',
      });
    }
    else {
      return res.status(404).json({
        message: 'Requested ID Not Found',
      });

    }
    // const taskIndex = tasksContainer.tasks;
    // tasksContainer.tasks.splice(taskIndex, 1);
  } else {
    return res.status(400).json({
      message: 'Bad Request / Error',
    });
  }
});

app.listen(9001, () => {
  process.stdout.write('the server is available on http://localhost:9001/\n');
});
